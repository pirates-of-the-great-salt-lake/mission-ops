# Mission Ops Package

This package is the tracking and home base command issuing server.
This is built in Python3 with Flask.

## Getting Started

To run get set up running this package, make sure you have Python3.7 or higher installed.

First you must should create a virtual environment by running `python3 -m venv .mission-ops-env`.
This will create a folder called `.mission-ops-env`, which is ignored by git.
Then run `. ./env`, which will put you into the python virtual environment.
Finally, install all the dependencies by running `pip install -r requirements.txt`.

To run the project, a simple `make` will work. 
