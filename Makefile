FLASK_ENTRY=src/main.py


.PHONY: verify_key

run: verify_key
run: export FLASK_APP=$(FLASK_ENTRY)
run:
	flask run 

verify_key: 
ifeq ("$(wildcard .aprsfi_key)","")
	$(error aprs.fi key missing)
else
	$(info aprs.fi key found)	
endif


env: 
	source .mission-ops/bin/activate

