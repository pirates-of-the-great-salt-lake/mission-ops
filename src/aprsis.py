import socket
import sys
import threading
import time

APRS_IS_SERVER = ("rotate.aprs2.net",14580)

PAK_POS = "pos"
PAK_MSG = "msg"
PAK_ACK = "ack"
PAK_UNK = "unk"

class Aprs_Packet:

    def __init__(self,text=None):
        self.reset()
        if text is not None:
            self._decode(text)

    def _decode_pos(self,text,start):
        self.type = PAK_POS
        self.lat = text[start:start + 8]
        start = start + 9
        self.lon = text[start:start + 9]

    def _decode_msg(self,text,start):
        self.type = PAK_MSG
        self.msg_addr = text[start:start+9]
        id_ind = text.find("{",start)
        self.msg_cont = text[start+10:id_ind]
        if id_ind != -1:
            self.msg_id = text[id_ind+1:]

    def _decode(self,text):
        try:
            self.text = text
            start = cursor = text.index(">")
            self.origin = text[:cursor]
            cursor = text.index(",")
            self.dest = text[start+1:cursor]
            start = cursor
            cursor = text.index(":",start)
            self.via = text[start+1:cursor].split(",")

            start = text.index(":") + 1

            identifier = text[start:start+1]

            if identifier == "!" or identifier == "=":
                self._decode_pos(text,start+1)
            elif identifier == ":":
                self._decode_msg(text,start+1)

        except Exception as e:
            print("Error:\t",e)
            print("Text:\t",text)


    def reset(self):
        self.origin = ""
        self.dest = ""
        self.via = []
        self.lat = ""
        self.lon = ""
        self.type = ""
        self.msg_addr = ""
        self.msg_id = ""
        self.msg_cont = ""
        self.text = ""

    def print(self):
        print("From:\t",self.origin)
        print("  To:\t",self.dest)
        print(" Via:\t",'|'.join(self.via))
        print("Type:\t",self.type)
        if self.type == PAK_POS:
            print("\t Lat:\t",self.lat)
            print("\t Lon:\t",self.lon)
            print("\t Raw:\t",self.text)
        elif self.type ==  PAK_MSG:
            print("\t  Id:\t",self.msg_id)
            print("\tAddr:\t",self.msg_addr)
            print("\tCont:\t",self.msg_cont)
        print("\n")
    def ack(pak, origin):
        ack_pak = Aprs_Packet()
        ack_pak.msg_id = pak.msg_id
        ack_pak.msg_addr = pak.origin
        ack_pak.origin = origin
        ack_pak.dest = "APZ000"
        return ack_pak

    "SMSGTE>APSMS1,TCPIP,qAS,VE3OTB-12::KJ7MKE   :ack0M}"
    def get_text(self):
        text = self.origin + ">" + self.dest
        text = text + ",TCPIP,qAS,KJ7MKE-7::"+(self.msg_addr.ljust(9))+":ack"+self.msg_id
        return text



class AprsCon:

    def _get_data(self):
        try:
            return self.sock.recv(256).decode("utf-8")
        except Exception as e:
            print("Unreadable -- for now")
        return None


    def _do_start(self):
        data = self._get_data()
        if "aprsc" in data:
            print("Valid Connection.")
            self.sock.sendall(b"user KJ7MKE-7 pass 17568\n")
            data = self.sock.recv(256).decode("utf-8")
            self.sock.sendall(b"# filter b/KJ7MKE/KJ7MKE-7\n")
            self._run()

    def _run(self):
        send = True
        data = self._get_data()

        if data is None:
            print("Error configuring connection")

        print("Recvd: ",data)
        while True:
            data = self._get_data()
            if data is None:
                pass
            elif data.startswith("#"):
                pass
            else:
                pak = Aprs_Packet(data)
                pak.print()
                if pak.type == PAK_MSG:
                    ack = Aprs_Packet.ack(pak,"KJ7MKE-7")
                    time.sleep(1.5)
                    print("Response:\t",ack.get_text())
                    self.sock.sendall(ack.get_text().encode('UTF-8'))


    def _connect(self):
        print("Starting connection...")
        self.sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.sock.connect(APRS_IS_SERVER)
        print("Connected.")
        self._do_start()

    def connect(self):
        self.thread = threading.Thread(target=AprsCon._connect,args=(self,))
        self.thread.start()
