from flask import Flask
from apscheduler.schedulers.background import BackgroundScheduler
import requests
from aprsis import AprsCon

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello, World!'

def periodic():
    pass

def init_scheduler():
    scheduler = BackgroundScheduler()
    job = scheduler.add_job(periodic, 'interval', seconds=5)
    scheduler.start()


def init():
    init_scheduler()
    con = AprsCon()
    con.connect()


init()
