import React from 'react';
import logo from './logo.svg';
import { Map, Marker, Polyline, Popup, TileLayer, LatLng } from 'react-leaflet'
import './App.css';
import BoatMap, { BoatReport, Waypoint, WaypointStatus } from './BoatMap';

const multiPolyline: Array<Array<LatLng>> = [
  [
    [40.71,-111.94],
    [40.72,-111.94],
    [40.73,-111.96],
  ],
  [
    [51.5, -0.05],
    [51.5, -0.06],
    [51.52, -0.06],
  ],
];

interface AppProps{

};

interface AppState{
  report:BoatReport;
  bread:BoatReport[];
  waypoints:Waypoint[];
};

export default class App extends React.Component<AppProps,AppState> {
  constructor(props:AppProps){
    super(props);
    this.state ={
      report: {
        time: new Date(Date.now()),
        battery: 68,
        position: [41.12,-112.4385],
        accuracy:26
      },
      bread: [
        {
          time: new Date(Date.now()),
          battery: 71,
          position: [41.09,-112.439],
          accuracy:14,
        },
        {
          time: new Date(Date.now()),
          battery: 72,
          position: [41.10,-112.441],
          accuracy:21,
        },
        {
          time: new Date(Date.now()),
          battery: 73,
          position: [41.11,-112.4415],
          accuracy:10,
        },
    ],
    waypoints: [
      {
        id:1,
        position: [41.10,-112.44],
        radius:90,
        status:WaypointStatus.Completed,
        timeComleted:new Date(Date.now() - 510000)
      },
      {
        id:2,
        position: [41.115,-112.44],
        radius:30,
        status:WaypointStatus.Completed,
        timeComleted:new Date(Date.now() - 500000)
      },
      {
        id:3,
        position: [41.1225,-112.44],
        radius:30,
        status:WaypointStatus.Target
      },
      {
        id:4,
        position: [41.125,-112.44],
        radius:30,
        status:WaypointStatus.Active
      },
      {
        id:5,
        position: [41.13,-112.44],
        radius:30,
        status:WaypointStatus.Active
      },

    ]
  };
  }

waypointMoved(id:number,pos:[number,number]){
  var ind = this.state.waypoints.findIndex(w => w.id == id);
  var copy = [...this.state.waypoints];
  copy[ind].position = pos;
  this.setState(s => {return {...this.state,waypoints:copy}});
  console.log("moved");
}

  render(){
    return (
      <BoatMap waypoints={this.state.waypoints} report={this.state.report} breadcrumbs={this.state.bread} editable onWaypointMoved={this.waypointMoved.bind(this)}/>
      );
    }
}
