import React from 'react';
import logo from './logo.svg';
import { Map, Marker, Circle, CircleMarker, Polyline, Popup, TileLayer, LatLng } from 'react-leaflet'
import './App.css';
import { DivIcon } from 'leaflet';


export interface BoatReport{
  position: [number,number];
  accuracy: number;
  battery: number;
  time: Date;
}

export enum WaypointStatus{
  Active = 1,
  Target = 2,
  Completed = 3,
  Abandoned = 4
}

export interface Waypoint{
  id: number;
  position: [number,number];
  radius:number;
  status:WaypointStatus;
  timeComleted?:Date;
}

export interface BoatMapProps{
  report: BoatReport;
  breadcrumbs?: BoatReport[];
  waypoints: Waypoint[];
  editable?: boolean;
  onWaypointMoved?:(id:number,pos:[number,number])=>void;
}

export default class BoatMap extends React.Component<BoatMapProps> {

  waypointRefs:{[id:number]:React.RefObject<any>} = {};

constructor(props:BoatMapProps){
  super(props);
  for(var i = 0; i < props.waypoints.length; i++){
    this.waypointRefs[props.waypoints[i].id] = React.createRef();
  }
}

  componentWillUpdate(nextProps:BoatMapProps){
    for(var i = 0; i < nextProps.waypoints.length; i++){
      this.waypointRefs[nextProps.waypoints[i].id] = React.createRef();
    }
  }

  getWaypoints():JSX.Element[]{

    var waypointMarkers = new Array<JSX.Element>();

    if(this.props.waypoints){

      for(var i = 0; i < this.props.waypoints.length; i++){

        var waypoint = this.props.waypoints[i];

        var waypointColor = "red";

        if(waypoint.status == WaypointStatus.Completed){
          waypointColor = "green"
        }else if(waypoint.status == WaypointStatus.Target){
          waypointColor = "blue";
        }else if(waypoint.status == WaypointStatus.Active){
            waypointColor = "gray";
        }

        waypointMarkers.push(
          <Marker icon={new DivIcon({html:'<svg height="10" width="10"><circle cx="5" cy="1" r="3" stroke="black" stroke-width="3" fill="'+waypointColor+'" /></svg>'})}  draggable={true} onDragend={this.updateWaypoint.bind(this,waypoint.id)} ref={this.waypointRefs[waypoint.id]}  position={waypoint.position}>
            <Circle color={waypointColor} stroke={false} center={waypoint.position} radius={waypoint.radius} />
            <Popup>Waypoint #{waypoint.id}<br/>{waypoint.timeComleted &&("Completed at: "+waypoint.timeComleted.toString())}<br/></Popup>
          </Marker>
        );
      }
    }
    return waypointMarkers;
  }

getBreadcrumbs():[[number,number][],JSX.Element[]]{
  var positions: [number,number][] = [];
  var breadcrumbMarkers = new Array<JSX.Element>();
  if(this.props.breadcrumbs){

    for(var i = 0; i < this.props.breadcrumbs.length; i++){

      var crumb = this.props.breadcrumbs[i];

      positions.push(crumb.position);

      breadcrumbMarkers.push(
        <CircleMarker color="black" fillOpacity={1} radius={3} center={crumb.position}>
          <Circle color="red" stroke={false} center={crumb.position} radius={crumb.accuracy} />
          <Popup>Time: {crumb.time.toString()}<br/>Battery: {crumb.battery}<br/></Popup>
        </CircleMarker>
      );
    }
  }
  positions.push(this.props.report.position);
  return [positions,breadcrumbMarkers];
}

updateWaypoint(id:number){
  console.log("updated");

  var pos = (this.waypointRefs[id].current as any).leafletElement.getLatLng() as [number,number];
  if(this.props.onWaypointMoved)
    this.props.onWaypointMoved(id,pos);
}

  render(){
    var items = this.getBreadcrumbs();
    var waypoints = this.getWaypoints();

    var completed:[number,number][] = [];
    var target:[number,number][] = [];
    var active:[number,number][] = [];

    if(this.props.waypoints){
      for(var i = 0; i < this.props.waypoints.length; i++){

        var waypoint = this.props.waypoints[i];
        var nextWaypoint = this.props.waypoints[i+1];

        if(waypoint.status == WaypointStatus.Completed){

          completed.push(waypoint.position);

          if(nextWaypoint && nextWaypoint.status == WaypointStatus.Target){
            target.push(waypoint.position);
          }
        }else if (waypoint.status == WaypointStatus.Target){
          target.push(waypoint.position);
          active.push(waypoint.position);
        }else if(waypoint.status == WaypointStatus.Active){
          active.push(waypoint.position);
        }
      }
    }

    return (
      <Map center={this.props.report.position} zoom={10}>
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
        />
        <Marker position={this.props.report.position}>
          <Popup>Time: {this.props.report.time.toString()}<br/>Battery: {this.props.report.battery}<br/></Popup>
        </Marker>
        <Polyline color="green" strokeWidth={1} lineCap="butt" dashArray="8 2" positions={items[0]} />
        <Polyline color="blue" strokeWidth={1} lineCap="butt" dashArray="8 2" positions={target} />
        <Polyline color="gray" strokeWidth={1} lineCap="butt" dashArray="8 2" positions={active} />
        <Polyline color="green" strokeWidth={1} lineCap="butt" dashArray="8 2" positions={completed} />



        {waypoints}
        {items[1]}

      </Map>
      );
    }
}
